// Shell `simplesh`
// Francisco José González Carrillo
// Valiantsin Kivachuk
// Grupo 1.2

#define _XOPEN_SOURCE 500

#include <unistd.h>
#include <string.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>

//#include <sys/types.h>
#include <sys/wait.h>

#include <sys/stat.h>
#include <pwd.h>

#include <libgen.h>

// Libreadline
#include <readline/readline.h>
#include <readline/history.h>

#include <errno.h>

//Directory methods
#include <ftw.h>
//#include <stdint.h>

// Tipos presentes en la estructura `cmd`, campo `type`.
#define EXEC  1
#define REDIR 2
#define PIPE  3
#define LIST  4
#define BACK  5

#define MAXARGS 15
#define BUF_SIZE 4096
#define BLOCK_SIZE 512


// Estructuras
// -----

// La estructura `cmd` se utiliza para almacenar la información que
// servirá al shell para guardar la información necesaria para
// ejecutar las diferentes tipos de órdenes (tuberías, redirecciones,
// etc.)
//
// El formato es el siguiente:
//
//     |----------+--------------+--------------|
//     | (1 byte) | ...          | ...          |
//     |----------+--------------+--------------|
//     | type     | otros campos | otros campos |
//     |----------+--------------+--------------|
//
// Nótese cómo las estructuras `cmd` comparten el primer campo `type`
// para identificar el tipo de la estructura, y luego se obtendrá un
// tipo derivado a través de *casting* forzado de tipo. Se obtiene así
// un polimorfismo básico en C.

struct cmd {
    int type;
};

// Ejecución de un comando con sus parámetros

struct execcmd {
    int type;
    char * argv[MAXARGS];
    char * eargv[MAXARGS];
};

// Ejecución de un comando de redirección

struct redircmd {
    int type;
    struct cmd *cmd;
    char *file;
    char *efile;
    int mode;
    int fd;
};

// Ejecución de un comando de tubería

struct pipecmd {
    int type;
    struct cmd *left;
    struct cmd *right;
};

// Lista de órdenes

struct listcmd {
    int type;
    struct cmd *left;
    struct cmd *right;
};

// Tarea en segundo plano (background) con `&`.

struct backcmd {
    int type;
    struct cmd *cmd;
};

// Declaración de funciones necesarias
int fork1(void); // Fork but panics on failure.
void panic(char*);
struct cmd *parse_cmd(char*);
void perror_exit(int exit_status, int exit_errno);
int preparse_input(struct cmd *cmd, char* command);

/**
 * Method to change directory
 * @param dir Dest directory.
 */
void modify_cwd(char * dir) {

    //    extern char * CWD;

    if (dir == 0) { //cd to $HOME dir
        char * home_path;
        if ((home_path = getenv("HOME")) == NULL) { //try to get $HOME variable
            perror("getenv $HOME error");
        } else { //We get $HOME path
            if (chdir(home_path) == -1) { //try to cd
                perror("chdir error");
            }
        }
    } else { //another path
        if (chdir(dir) == -1) { //try to cd
            perror("chdir error");
        }
    }
}

void run_tee(struct cmd *cmd) {
    struct execcmd *ecmd;

    ecmd = (struct execcmd*) cmd;

    int opt, argc = 0;

    int flags = O_CREAT | O_WRONLY | O_TRUNC; //default flags to open files

    for (int i = 0; ecmd->argv[i]; i++) //get number of args
        argc++;

    while ((opt = getopt(argc, ecmd->argv, "ha")) != -1) {
        switch (opt) {
            case 'h':
                printf("Uso : tee [ - h ] [ - a ] [ FICHERO ]...\r\n"
                        "\tCopia stdin a cada FICHERO y a stdout.\r\n"
                        "\tOpciones :\r\n"
                        "\t-a Añade al final de cada FICHERO\r\n"
                        "\t-h help\r\n");
                return;
                break;
            case 'a':
                flags = O_CREAT | O_WRONLY | O_APPEND; //add append flag
                break;
            case '?': //args error
                return;
        }
    }

    int num_files = argc - optind; //total de ficheros en los que escribir
    int opened_files = 0; //ficheros que hemos conseguido abrir
    char buf[BUF_SIZE]; //input buffer
    int FILES[num_files]; //puntero de los ficheros

    if (num_files > 0) { //if exist files to open
        for (int index = optind; index < argc; index++) { //asignamos a FILES el nombre de cada archivo

            int descriptor = -1;

            if ((descriptor = open(ecmd->argv[index], flags, 0755)) != -1) { //try to get descriptor
                FILES[opened_files] = descriptor;
                opened_files++;
            } else {
                perror("open error");
            }
        }
    }
    int readed, writed = -2;
    int cumwrited;

    while ((readed = read(STDIN_FILENO, buf, sizeof (buf))) != 0) { //read buffer
        if (readed == -1) {
            perror("read error");
        } else {

            cumwrited = 0;
            while (readed > cumwrited) {
                if ((writed = write(STDOUT_FILENO, buf, readed)) == -1) {
                    perror("write error");
                }
                cumwrited += writed;
            }

            for (int index = 0; index < opened_files; index++) { //write buffer for each file

                cumwrited = 0;
                while (readed > cumwrited) {
                    if ((writed = write(FILES[index], buf, readed)) == -1) {
                        perror("write error");
                    }
                    cumwrited += writed;
                }
            }
        }
    }

    for (int index = 0; index < opened_files; index++) { //only for opened files
        if (fsync(FILES[index]) == -1) {
            perror("fsync error");
        }
        if (close(FILES[index]) == -1) {
            perror("write error");
        }
    }

    char * home_path;
    if ((home_path = getenv("HOME")) == NULL) { //try to get $HOME variable
        perror("getenv $HOME error");
    } else {
        char *file = malloc(sizeof (char) * BUF_SIZE);

        perror_exit(
                (file == NULL) ? -1 : 0, //if malloc fails, pass -1 directly
                errno
                );

        if (snprintf(file, BUF_SIZE, "%s/.tee.log", home_path) > BUF_SIZE) { //concatenate
            perror(".tee.log buf limit was reached");
        } else {
            FILE * fd;
            if ((fd = fopen(file, "a+")) == NULL) { //open file descriptor
                perror("fopen .tee.log error");
            } else {

                time_t tiempo;
                char buffer[BUF_SIZE];
                struct tm* tm_info;

                perror_exit(
                        time(&tiempo), //get time
                        errno
                        );

                if ((tm_info = localtime(&tiempo)) == NULL) { //parse timestamp

                } else {

                    strftime(buffer, BUF_SIZE, "%Y-%m-%d %H:%M:%S", tm_info);

                    perror_exit(
                            fprintf(fd, "%s:PID %d:EUID %o:%d byte(s):%d file(s)\n", buffer, getpid(), geteuid(), cumwrited, opened_files),
                            errno
                            );

                    perror_exit(
                            fclose(fd),
                            errno
                            );

                }
            }
        }

        free(file);
    }



}

long size_summary = 0; //sumatorio de tamaño
char use_block_size = '0'; //"booleano"  para usar tamaño de fichero o de disco
long file_limit = 0; //-t option
int verbose = 0; //-v option

int nftw_callback_dir(const char *fpath, const struct stat *sb, int tflag, struct FTW * ftwbuf) {

    if (tflag == FTW_D) {

        if (verbose) {
            for (int count = 0; count < ftwbuf->level; count++) {
                printf("   ");
            }

            printf("%s\n", fpath);

        }
    } else if (tflag == FTW_F || tflag == FTW_SL) {
        if ((file_limit == 0) //if no limit exist
                || ((file_limit > 0 && file_limit > sb->st_size) //files bigger than file_limit
                || (file_limit < 0 && -file_limit < sb->st_size))) { //files smaller than file_limit


            if (verbose) {
                for (int count = 0; count < ftwbuf->level; count++) {
                    printf("   ");
                }

                if (use_block_size == '0') { //real size
                    size_summary += sb->st_size;
                    printf("%s: %li\n", fpath, sb->st_size);
                } else { //block size
                    size_summary += BLOCK_SIZE * sb->st_blocks;
                    printf("%s: %li\n", fpath, BLOCK_SIZE * sb->st_blocks);
                }
            }
        }
    }
    return 0; /* To tell nftw() to continue */
}

void run_du(struct cmd *cmd) {
    struct execcmd *ecmd;

    ecmd = (struct execcmd*) cmd;

    int opt, argc = 0;

    for (int i = 0; ecmd->argv[i]; i++)
        argc++;

    while ((opt = getopt(argc, ecmd->argv, "hbt:v")) != -1) {
        switch (opt) {
            case 'h':
                printf("Uso : du [ - h ] [ - b ] [ - t SIZE ] [ - v ] [ FICHERO | DIRECTORIO ]...\r\n"
                        "Para cada fichero, imprime su tama ñ o .\r\n"
                        "Para cada directorio, imprime la suma de los tama ñ os de todos los ficheros de\r\n"
                        "      todos sus subdirectorios.\r\n"
                        "   Opciones:\r\n"
                        "   -b Imprime el tamaño ocupado en disco por todos los bloques del fichero.\r\n"
                        "   -t SIZE Excluye todos los ficheros más pequeños que SIZE bytes, si es\r\n"
                        "      negativo, o más grandes que SIZE bytes, si es positivo, cuando se\r\n"
                        "      procesa un directorio.\r\n"
                        "   -v Imprime el tamaño de todos y cada uno de los ficheros cuando se procesa un\r\n"
                        "      directorio.\r\n"
                        "   -h help\r\n"
                        "Nota : Todos los tamaños están expresados en bytes.\r\n");
                return;
                break;
            case 'b':
                use_block_size = '1';
                break;
            case 't':
                file_limit = atol(optarg);
                break;
            case 'v':
                verbose = 1;
                break;
            case '?': //args error
                return;
        }
    }

    int num_files = argc - optind; //total de ficheros en los que escribir
    int valid_files = 0; //ficheros que hemos conseguido coger la informacion
    int max_path_simultaneous = 20; //how many descriptors can open simultaneosly

    struct file_info {
        struct stat file_stat; //stat of file
        char * file_name; //name of file
    } file_info[(num_files == 0) ? 1 : num_files]; //when no args for du, extra slot for "du ."

    int descriptor = -1;

    if (num_files > 0) { //when existing files
        for (int index = optind; index < argc; index++) { //asignamos a FILES el nombre de cada archivo

            if ((descriptor = stat(ecmd->argv[index], &file_info[valid_files].file_stat)) != -1) {
                file_info[valid_files].file_name = ecmd->argv[index];
                valid_files++;
            } else {
                perror("stat error");
            }
        }
    } else { //du .
        if ((descriptor = stat(".", &file_info[valid_files].file_stat)) != -1) {
            file_info[valid_files].file_name = ".";
            valid_files++;
        } else {
            perror("stat error");
        }
    }

    for (int index = 0; index < valid_files; index++) {

        perror_exit(
                nftw(file_info[index].file_name, nftw_callback_dir, max_path_simultaneous, FTW_PHYS),
                errno
                );

        if (S_ISDIR(file_info[index].file_stat.st_mode)) {
            printf("(D) %s: %ld\n", file_info[index].file_name, size_summary);
        } else if (S_ISREG(file_info[index].file_stat.st_mode)) {
            printf("(F) %s: %ld\n", file_info[index].file_name, size_summary);
        }

        size_summary = 0;

    }

}

// Ejecuta un `cmd`. Nunca retorna, ya que siempre se ejecuta en un
// hijo lanzado con `fork()`.

void
run_cmd(struct cmd *cmd) {
    int p[2];
    struct backcmd *bcmd;
    struct execcmd *ecmd;
    struct listcmd *lcmd;
    struct pipecmd *pcmd;
    struct redircmd *rcmd;

    if (cmd == 0)
        exit(0);

    switch (cmd->type) {
        default:
            panic("run_cmd");

            // Ejecución de una única orden.
        case EXEC:
            ecmd = (struct execcmd*) cmd;
            if (ecmd->argv[0] == 0)
                exit(0);

            if (strcmp(ecmd->argv[0], "pwd") == 0) { // busca si es pwd
                char actualPATH[BUF_SIZE];
                if (getcwd(actualPATH, BUF_SIZE) != NULL) {
                    fprintf(stderr, "simplesh: pwd: ");
                    fprintf(stdout, "%s\n", actualPATH);
                } else {
                    perror("getcwd() error");
                }

            } else if (strcmp(ecmd->argv[0], "cd") == 0) {
                modify_cwd(ecmd->argv[1]);
            } else if (strcmp(ecmd->argv[0], "tee") == 0) {
                run_tee(cmd);
            } else if (strcmp(ecmd->argv[0], "du") == 0) {
                run_du(cmd);
            } else if (strcmp(ecmd->argv[0], "exit") == 0) {
                return;
            } else {

                execvp(ecmd->argv[0], ecmd->argv);

                // Si se llega aquí algo falló
                fprintf(stderr, "exec %s failed\n", ecmd->argv[0]);
                exit(1);
            }
            break;

        case REDIR:
            rcmd = (struct redircmd*) cmd;
            close(rcmd->fd);
            if (open(rcmd->file, rcmd->mode, S_IRUSR | S_IWUSR) < 0) {
                fprintf(stderr, "open %s failed\n", rcmd->file);
                exit(1);
            }
            run_cmd(rcmd->cmd);
            break;

        case LIST:
            lcmd = (struct listcmd*) cmd;
            if (fork1() == 0)
                run_cmd(lcmd->left);
            wait(NULL);
            preparse_input(lcmd->left, "cd");
            run_cmd(lcmd->right);
            break;

        case PIPE:
            pcmd = (struct pipecmd*) cmd;
            if (pipe(p) < 0)
                panic("pipe");

            // Ejecución del hijo de la izquierda
            if (fork1() == 0) {
                close(1);
                dup(p[1]);
                close(p[0]);
                close(p[1]);
                run_cmd(pcmd->left);
            }

            // Ejecución del hijo de la derecha
            if (fork1() == 0) {
                close(0);
                dup(p[0]);
                close(p[0]);
                close(p[1]);
                run_cmd(pcmd->right);
            }
            close(p[0]);
            close(p[1]);

            // Esperar a ambos hijos
            wait(NULL);
            wait(NULL);
            break;

        case BACK:
            bcmd = (struct backcmd*) cmd;
            if (fork1() == 0)
                run_cmd(bcmd->cmd);

            break;
    }

    // Salida normal, código 0.
    exit(0);
}

// Muestra un *prompt* y lee lo que el usuario escribe usando la
// librería readline. Ésta permite almacenar en el historial, utilizar
// las flechas para acceder a las órdenes previas, búsquedas de
// órdenes, etc.

char*
getcmd() {
    char *buf;

    uid_t uid = getuid();
    struct passwd *pwuid = getpwuid(uid);
    if (pwuid == NULL) {
        perror("getpwuid");
        exit(EXIT_FAILURE);
    }

    char actualPATH[BUF_SIZE];

    if (getcwd(actualPATH, BUF_SIZE) == NULL) {
        perror("getcwd");
        exit(EXIT_FAILURE);
    }

    char *prompt = malloc(sizeof (char) * BUF_SIZE);

    perror_exit(
            (prompt == NULL) ? -1 : 0, //if malloc fails, pass -1 directly
            errno
            );

    if (snprintf(prompt, BUF_SIZE, "%s@%s$ ", pwuid->pw_name, basename(actualPATH)) > BUF_SIZE) {
        perror("cwd limit was reached");
    }

    // Lee la entrada del usuario
    buf = readline(prompt);

    // Si el usuario ha escrito algo, almacenarlo en la historia.
    if (buf) {
        add_history(buf);
    }

    free(prompt);

    return buf;
}

// Definiciones adelantadas de funciones.
struct cmd *parse_line(char**, char*);
struct cmd *parse_pipe(char**, char*);
struct cmd *parse_exec(char**, char*);
struct cmd *nulterminate(struct cmd*);

/**
 * Recorre toda la estructura para eliminar todas las ejecuciones de forma recursiva
 */
void delete_executions(struct cmd *cmd) {
    int i;
    struct backcmd *bcmd;
    struct execcmd *ecmd;
    struct listcmd *lcmd;
    struct pipecmd *pcmd;
    struct redircmd *rcmd;

    switch (cmd->type) {
        case EXEC:
            ecmd = (struct execcmd*) cmd;
            for (i = 0; ecmd->argv[i]; i++)
                ecmd->argv[i] = '\0';
            break;

        case REDIR:
            rcmd = (struct redircmd*) cmd;
            delete_executions(rcmd->cmd);
            *rcmd->file = 0;
            break;

        case PIPE:
            pcmd = (struct pipecmd*) cmd;
            nulterminate(pcmd->left);
            nulterminate(pcmd->right);
            break;

        case LIST:
            lcmd = (struct listcmd*) cmd;
            delete_executions(lcmd->left);
            delete_executions(lcmd->right);
            break;

        case BACK:
            bcmd = (struct backcmd*) cmd;
            delete_executions(bcmd->cmd);

            break;
    }

}

/**
 * Recorre la estructura. y realiza accion
 * @param cmd Estructura
 * @param command cd o exit
 * @return 0 si no se ha encontrado. 1 si está al principio. 2 si está en medio de otras instrucciones
 */
int preparse_input(struct cmd *cmd, char* command) {
    struct execcmd *ecmd;
    struct listcmd *lcmd;

    switch (cmd->type) {
        default:
            panic("check_exit");

            return -1;
        case EXEC:
            ecmd = (struct execcmd*) cmd;
            if (ecmd->argv[0] == 0)
                return 0;


            if (strcmp(command, "cd") == 0) {
                if (strcmp(ecmd->argv[0], command) == 0) {
                    modify_cwd(ecmd->argv[1]);
                }
                return 0;
            } else if (strcmp(command, "exit") == 0) {
                if (strcmp(ecmd->argv[0], command) == 0) {
                    return 1; //no se hace exit para que se pueda liberar recursos
                } else {
                    return 0; //no hemos encontrado exit
                }
            } else {
                return -1;
            }
            break;
        case LIST:
            lcmd = (struct listcmd*) cmd;
            int exit_status = 0, left_return, right_return;

            if ((left_return = preparse_input(lcmd->left, command)) != 0) {

                if (left_return == 1) { //Ha encontrado un exit y no es de una llamada recursiva por debajo
                    delete_executions(cmd);
                }
                exit_status++; //Si left_return no era 1, significa que se eliminó un exit en otra llamada, por lo que devolvemos esta info hacia arriba


            } else if ((right_return = preparse_input(lcmd->right, command)) != 0) {

                if (right_return == 1) { //Ha encontrado un exit y no es de una llamada recursiva por debajo
                    delete_executions(cmd);
                }
                exit_status++; //Si left_return no era 1, significa que se eliminó un exit en otra llamada, por lo que devolvemos esta info hacia arriba

            }

            return (exit_status == 0) ? 0 : 2;

            break;
        case PIPE:
            return 0;
        case BACK:
            return 0;
        case REDIR:
            return 0;
            break;

    }
}

//char * CWD; //Actual Directory

// Función `main()`.
// ----
int count = 0;
int timeout = 5;

void signal_handler(int sig) {
    /* SIGINT (^ C ) : increase the counter */

    if (sig == SIGCHLD) {
        count++;
        return;
    } else if (sig == SIGUSR1) {
        timeout += 5;
        //   printf("incrementando en 5: %d\n", timeout);
        return;
    } else if (sig == SIGUSR2) {
        timeout -= (timeout > 0) ? 5 : 0;
        //   printf("decrementando en 5: %d\n", timeout);
        return;
    }

    /* SIGQUIT (^\) : terminate the process */
    /* exit is not an async - signal - safe call */

    _exit(EXIT_SUCCESS);

}

struct timespec GetTimeStamp() {
    struct timespec ts;

    perror_exit(
            clock_gettime(TIME_UTC, &ts),
            errno
            );
    return ts;
}

void perror_exit(int exit_status, int exit_errno) {
    // Keep in mind that fprintf(stderr, "%s\n", strerror(errno)) is similar to perror(NULL)
    // http://stackoverflow.com/questions/12102332/when-should-i-use-perror-and-fprintfstderr
    if (exit_status == -1) {
        errno = exit_errno;
        perror(NULL);
        exit(EXIT_FAILURE);
    }
}

int main(void) {

    struct sigaction sa;
    sa.sa_handler = signal_handler;

    perror_exit(
            sigemptyset(&sa.sa_mask), //limpiamos el conjuntos
            errno
            );

    sigset_t blocked_signals;

    perror_exit(
            sigemptyset(&blocked_signals), //limpiamos el conjunto
            errno
            );

    perror_exit(
            sigaddset(&blocked_signals, SIGINT), //agregamos elemento
            errno
            );

    perror_exit(
            sigaddset(&blocked_signals, SIGCHLD), //agregamos elemento
            errno
            );


    //Creamos otro conjunto exclusivo para SIGCHLD copiando el anterior y excluyendo SIGINT
    sigset_t unblockable_signals = blocked_signals;
    perror_exit(
            sigdelset(&unblockable_signals, SIGINT),
            errno
            );

    struct timespec tiempo;


    perror_exit(
            sigprocmask(SIG_BLOCK, &blocked_signals, NULL),
            errno
            );

    perror_exit(//logical or to return values
            sigaction(SIGCHLD, & sa, NULL) | sigaction(SIGUSR1, & sa, NULL) | sigaction(SIGUSR2, & sa, NULL),
            errno
            );

    char* buf;

    struct cmd* input_parsed;
    // Bucle de lectura y ejecución de órdenes.
    while (NULL != (buf = getcmd())) {
        input_parsed = parse_cmd(buf);
        int status = preparse_input(input_parsed, "exit");

        if (status == 1) { //si solo tenemos un comando exit
            free((void*) buf);
            exit(0);
        } else if (status == -1) {
            exit(-1);
        }

        int pid, cosas;
        // Crear siempre un hijo para ejecutar el comando leído
        if ((pid = fork1()) == 0) {
            run_cmd(input_parsed);
        } else {
            //https://www.linuxprogrammingblog.com/code-examples/signal-waiting-sigtimedwait
            int fin = 1;
            int error;
            struct timespec temporal;
            unsigned long timestamp;
            unsigned long tiempo_transcurrido = 0;
            tiempo.tv_sec = timeout;
            tiempo.tv_nsec = 0;
            while (fin) {
                temporal = GetTimeStamp();
                timestamp = temporal.tv_sec * 1000000 + temporal.tv_nsec / 1000;
                if (sigtimedwait(&unblockable_signals, NULL, &tiempo) == -1) {
                    error = errno;
                    if (error == EINTR) {
                        //Se detecta una llamada al sistema distinta y volvemos a ejecutar el bucle
                        //Lo hacemos en microsegundos para ahorrarnos el riesgo de overflow
                        temporal = GetTimeStamp();
                        tiempo_transcurrido += temporal.tv_sec * 1000000 + temporal.tv_nsec / 1000 - timestamp;
                        timestamp = timeout * 1000000;
                        if (tiempo_transcurrido < timestamp) {
                            tiempo.tv_sec = (timestamp - tiempo_transcurrido) / 1000000;
                            tiempo.tv_nsec = (timestamp - tiempo_transcurrido) % 1000000 * 1000;
                        } else {
                            tiempo.tv_sec = 0;
                            tiempo.tv_nsec = 0;
                        }
                        //si es negativo, lo dejamos a 0

                    } else if (error == EAGAIN) {
                        //Damos el paso al manejador, desvloqueando la señal, esperando, y volviendo a bloquearse
                        perror_exit(
                                sigprocmask(SIG_UNBLOCK, &unblockable_signals, NULL), //desbloqueamos
                                errno
                                );

                        perror_exit(
                                kill(pid, SIGKILL), //matamos al hijo
                                errno
                                );

                        perror_exit(
                                waitpid(pid, &cosas, 0), //esperamos a que el handler termine
                                errno
                                );

                        fprintf(stderr, "simplesh : [ %d ] Matado hijo con PID %d\n", count, pid);

                        perror_exit(
                                sigprocmask(SIG_BLOCK, &unblockable_signals, NULL), //volvemos a bloquear la señal
                                errno
                                );

                        fin = 0;

                    } else {
                        perror("sigtimedwait");
                        //                        return EXIT_FAILURE;
                    }
                } else {

                    perror_exit(
                            waitpid(pid, &cosas, 0),
                            errno
                            );

                    fin = 0;
                }
            }
        }
        // Esperar al hijo creado
        //wait(NULL);

        //        modify_cwd(CWD); //Possibly directory change after execution

        preparse_input(input_parsed, "cd");

        free((void*) buf);

        if (status == 2) { //si hemos ejecutado varios comandos y luego un exit
            exit(EXIT_SUCCESS);
        }

    }

    return 0;
}

void
panic(char *s) {

    fprintf(stderr, "%s\n", s);
    exit(-1);
}

// Como `fork()` salvo que muestra un mensaje de error si no se puede
// crear el hijo.

int
fork1(void) {
    int pid;

    pid = fork();
    if (pid == -1) {
        panic("fork");
    }
    return pid;
}

// Constructores de las estructuras `cmd`.
// ----

// Construye una estructura `EXEC`.

struct cmd*
execcmd(void) {
    struct execcmd *cmd;

    cmd = malloc(sizeof (*cmd));
    memset(cmd, 0, sizeof (*cmd));
    cmd->type = EXEC;

    return (struct cmd*) cmd;
}

// Construye una estructura de redirección.

struct cmd*
redircmd(struct cmd *subcmd, char *file, char *efile, int mode, int fd) {
    struct redircmd *cmd;

    cmd = malloc(sizeof (*cmd));
    memset(cmd, 0, sizeof (*cmd));
    cmd->type = REDIR;
    cmd->cmd = subcmd;
    cmd->file = file;
    cmd->efile = efile;
    cmd->mode = mode;
    cmd->fd = fd;

    return (struct cmd*) cmd;
}

// Construye una estructura de tubería (*pipe*).

struct cmd*
pipecmd(struct cmd *left, struct cmd *right) {
    struct pipecmd *cmd;

    cmd = malloc(sizeof (*cmd));
    memset(cmd, 0, sizeof (*cmd));
    cmd->type = PIPE;
    cmd->left = left;
    cmd->right = right;

    return (struct cmd*) cmd;
}

// Construye una estructura de lista de órdenes.

struct cmd*
listcmd(struct cmd *left, struct cmd *right) {
    struct listcmd *cmd;

    cmd = malloc(sizeof (*cmd));
    memset(cmd, 0, sizeof (*cmd));
    cmd->type = LIST;
    cmd->left = left;
    cmd->right = right;

    return (struct cmd*) cmd;
}

// Construye una estructura de ejecución que incluye una ejecución en
// segundo plano.

struct cmd*
backcmd(struct cmd *subcmd) {
    struct backcmd *cmd;

    cmd = malloc(sizeof (*cmd));
    memset(cmd, 0, sizeof (*cmd));
    cmd->type = BACK;
    cmd->cmd = subcmd;

    return (struct cmd*) cmd;
}

// Parsing
// ----

const char whitespace[] = " \t\r\n\v";
const char symbols[] = "<|>&;()";

// Obtiene un *token* de la cadena de entrada `ps`, y hace que `q` apunte a
// él (si no es `NULL`).

int
gettoken(char **ps, char *end_of_str, char **q, char **eq) {
    char *s;
    int ret;

    s = *ps;
    while (s < end_of_str && strchr(whitespace, *s))
        s++;
    if (q)
        *q = s;
    ret = *s;
    switch (*s) {
        case 0:
            break;
        case '|':
        case '(':
        case ')':
        case ';':
        case '&':
        case '<':
            s++;
            break;
        case '>':
            s++;
            if (*s == '>') {
                ret = '+';
                s++;
            }
            break;

        default:
            // El caso por defecto (no hay caracteres especiales) es el de un
            // argumento de programa. Se retorna el valor `'a'`, `q` apunta al
            // mismo (si no era `NULL`), y `ps` se avanza hasta que salta todos
            // los espacios **después** del argumento. `eq` se hace apuntar a
            // donde termina el argumento. Así, si `ret` es `'a'`:
            //
            //     |-----------+---+---+---+---+---+---+---+---+---+-----------|
            //     | (espacio) | a | r | g | u | m | e | n | t | o | (espacio) |
            //     |-----------+---+---+---+---+---+---+---+---+---+-----------|
            //                   ^                                   ^
            //                   |q                                  |eq
            //
            ret = 'a';
            while (s < end_of_str && !strchr(whitespace, *s) && !strchr(symbols, *s))
                s++;
            break;
    }

    // Apuntar `eq` (si no es `NULL`) al final del argumento.
    if (eq)
        *eq = s;

    // Y finalmente saltar los espacios en blanco y actualizar `ps`.
    while (s < end_of_str && strchr(whitespace, *s))
        s++;
    *ps = s;

    return ret;
}

// La función `peek()` recibe un puntero a una cadena, `ps`, y un final de
// cadena, `end_of_str`, y un conjunto de tokens (`toks`). El puntero
// pasado, `ps`, es llevado hasta el primer carácter que no es un espacio y
// posicionado ahí. La función retorna distinto de `NULL` si encuentra el
// conjunto de caracteres pasado en `toks` justo después de los posibles
// espacios.

int
peek(char **ps, char *end_of_str, char *toks) {
    char *s;

    s = *ps;
    while (s < end_of_str && strchr(whitespace, *s))
        s++;
    *ps = s;

    return *s && strchr(toks, *s);
}


// Función principal que hace el *parsing* de una línea de órdenes dada por
// el usuario. Llama a la función `parse_line()` para obtener la estructura
// `cmd`.

struct cmd*
parse_cmd(char *s) {
    char *end_of_str;
    struct cmd *cmd;

    end_of_str = s + strlen(s);
    cmd = parse_line(&s, end_of_str);

    peek(&s, end_of_str, "");
    if (s != end_of_str) {
        fprintf(stderr, "restante: %s\n", s);
        panic("syntax");
    }

    // Termina en `'\0'` todas las cadenas de caracteres de `cmd`.
    nulterminate(cmd);

    return cmd;
}

// *Parsing* de una línea. Se comprueba primero si la línea contiene alguna
// tubería. Si no, puede ser un comando en ejecución con posibles
// redirecciones o un bloque. A continuación puede especificarse que se
// ejecuta en segundo plano (con `&`) o simplemente una lista de órdenes
// (con `;`).

struct cmd*
parse_line(char **ps, char *end_of_str) {
    struct cmd *cmd;

    cmd = parse_pipe(ps, end_of_str);
    while (peek(ps, end_of_str, "&")) {
        gettoken(ps, end_of_str, 0, 0);
        cmd = backcmd(cmd);
    }

    if (peek(ps, end_of_str, ";")) {

        gettoken(ps, end_of_str, 0, 0);
        cmd = listcmd(cmd, parse_line(ps, end_of_str));
    }

    return cmd;
}

// *Parsing* de una posible tubería con un número de órdenes.
// `parse_exec()` comprobará la orden, y si al volver el siguiente *token*
// es un `'|'`, significa que se puede ir construyendo una tubería.

struct cmd*
parse_pipe(char **ps, char *end_of_str) {
    struct cmd *cmd;

    cmd = parse_exec(ps, end_of_str);
    if (peek(ps, end_of_str, "|")) {

        gettoken(ps, end_of_str, 0, 0);
        cmd = pipecmd(cmd, parse_pipe(ps, end_of_str));
    }

    return cmd;
}


// Construye los comandos de redirección si encuentra alguno de los
// caracteres de redirección.

struct cmd*
parse_redirs(struct cmd *cmd, char **ps, char *end_of_str) {
    int tok;
    char *q, *eq;

    // Si lo siguiente que hay a continuación es una redirección...
    while (peek(ps, end_of_str, "<>")) {
        // La elimina de la entrada
        tok = gettoken(ps, end_of_str, 0, 0);

        // Si es un argumento, será el nombre del fichero de la
        // redirección. `q` y `eq` tienen su posición.
        if (gettoken(ps, end_of_str, &q, &eq) != 'a')
            panic("missing file for redirection");

        switch (tok) {
            case '<':
                cmd = redircmd(cmd, q, eq, O_RDONLY, 0);
                break;
            case '>':
                cmd = redircmd(cmd, q, eq, O_RDWR | O_CREAT | O_TRUNC, 1);
                break;
            case '+': // >>
                cmd = redircmd(cmd, q, eq, O_RDWR | O_CREAT | O_APPEND, 1);

                break;
        }
    }

    return cmd;
}

// *Parsing* de un bloque de órdenes delimitadas por paréntesis.

struct cmd*
parse_block(char **ps, char *end_of_str) {
    struct cmd *cmd;

    // Esperar e ignorar el paréntesis
    if (!peek(ps, end_of_str, "("))
        panic("parse_block");
    gettoken(ps, end_of_str, 0, 0);

    // Parse de toda la línea hsta el paréntesis de cierre
    cmd = parse_line(ps, end_of_str);

    // Elimina el paréntesis de cierre
    if (!peek(ps, end_of_str, ")"))
        panic("syntax - missing )");
    gettoken(ps, end_of_str, 0, 0);

    // ¿Posibles redirecciones?
    cmd = parse_redirs(cmd, ps, end_of_str);

    return cmd;
}

// Hace en *parsing* de una orden, a no ser que la expresión comience por
// un paréntesis. En ese caso, se inicia un grupo de órdenes para ejecutar
// las órdenes de dentro del paréntesis (llamando a `parse_block()`).

struct cmd*
parse_exec(char **ps, char *end_of_str) {
    char *q, *eq;
    int tok, argc;
    struct execcmd *cmd;
    struct cmd *ret;

    // ¿Inicio de un bloque?
    if (peek(ps, end_of_str, "("))
        return parse_block(ps, end_of_str);

    // Si no, lo primero que hay una línea siempre es una orden. Se
    // construye el `cmd` usando la estructura `execcmd`.
    ret = execcmd();
    cmd = (struct execcmd*) ret;

    // Bucle para separar los argumentos de las posibles redirecciones.
    argc = 0;
    ret = parse_redirs(ret, ps, end_of_str);
    while (!peek(ps, end_of_str, "|)&;")) {
        if ((tok = gettoken(ps, end_of_str, &q, &eq)) == 0)
            break;

        // Aquí tiene que reconocerse un argumento, ya que el bucle para
        // cuando hay un separador
        if (tok != 'a')
            panic("syntax");

        // Apuntar el siguiente argumento reconocido. El primero será la
        // orden a ejecutar.
        cmd->argv[argc] = q;
        cmd->eargv[argc] = eq;
        argc++;
        if (argc >= MAXARGS)
            panic("too many args");

        // Y de nuevo apuntar posibles redirecciones
        ret = parse_redirs(ret, ps, end_of_str);
    }

    // Finalizar las líneas de órdenes
    cmd->argv[argc] = 0;
    cmd->eargv[argc] = 0;

    return ret;
}

// Termina en NUL todas las cadenas de `cmd`.

struct cmd*
nulterminate(struct cmd *cmd) {
    int i;
    struct backcmd *bcmd;
    struct execcmd *ecmd;
    struct listcmd *lcmd;
    struct pipecmd *pcmd;
    struct redircmd *rcmd;

    if (cmd == 0)
        return 0;

    switch (cmd->type) {
        case EXEC:
            ecmd = (struct execcmd*) cmd;
            for (i = 0; ecmd->argv[i]; i++)
                *ecmd->eargv[i] = 0;
            break;

        case REDIR:
            rcmd = (struct redircmd*) cmd;
            nulterminate(rcmd->cmd);
            *rcmd->efile = 0;
            break;

        case PIPE:
            pcmd = (struct pipecmd*) cmd;
            nulterminate(pcmd->left);
            nulterminate(pcmd->right);
            break;

        case LIST:
            lcmd = (struct listcmd*) cmd;
            nulterminate(lcmd->left);
            nulterminate(lcmd->right);
            break;

        case BACK:
            bcmd = (struct backcmd*) cmd;
            nulterminate(bcmd->cmd);
            break;
    }

    return cmd;
}

/*
 * Local variables:
 * mode: c
 * c-basic-offset: 4
 * fill-column: 75
 * eval: (auto-fill-mode t)
 * End:
 */
